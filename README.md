# steps to deploy sample Hello app in Kubernetes.
1- Make sure you have a kubernetes cluster up and running.
2- Simply connect to your cluster and clone the  repo, and go inside k8s directory.
3- Create a namespace inside your cluster with command:-
```
 kubectl create namespace hello-app-namespace
```
4- To deploy the application run the command:-
```
 kubectl apply -f deployment.yaml -n hello-app-namespace
```
5- To check deployment run the command:-
```
 kubetl get all -n hello-app-namespace
```
6- This deployment file have a service type Loadbalancer so you'll be getting an external ip.
7- Copy and paste the external ip on web-browser to check the application.

## To check rolling update on app  change the image url inside deployment.yaml file with :-
**Version 1 of sample hello app :-** 
```
 us-docker.pkg.dev/google-samples/containers/gke/hello-app:1.0
```
**Version 2 of sample hello app :-** 
```
 us-docker.pkg.dev/google-samples/containers/gke/hello-app:2.0
```

# Steps to create pub/sub topic using terraform.
1- Firstly create a service account for terraform in your gcp cloud, and assign  permission to create a pub/sub topic, and read/write access to gcs bucket.
2- After assigning permission create a json key for the terraform service account you created.
3- Fork the  repo and go to your project settings and under CI/CD variables add the json key as:-
```
keyname as - GOOGLE_CREDENTIALS
value as - paste your json key
```
4- Navigate to create-pub-sub and change the values in input.auto.tfvars file.
```
Replace your project_id  
```
5- Then run the pipeline job. The job have three stages:-
```
stage 1- plan
stage 2- apply

***It then ask for a value pass it as ***

DESTROY_OPTION = false
stage 3- destroy
```
6- By default destroy stage dont execute so as to execute this stage you need to provide a boolean value for variable:-
```
DESTROY_OPTION = true
```
**The terraform state file is automatically  stored in gcs bucket inside your gcp project.
**
