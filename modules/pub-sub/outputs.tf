output "topic_id" {
  description = "ID of the created topic."
  value       = google_pubsub_topic.topic.id
}

output "subscription_id" {
  description = "ID of the created subscription."
  value       = try(google_pubsub_subscription.pubsub_subscription.0.id, null)
}

output "push_subscription_id" {
  description = "ID of the created push subscription."
  value       = try(google_pubsub_subscription.pubsub_push_subscription.0.id, null)
}