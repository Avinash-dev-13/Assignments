#Local variables#

locals {
  destination_uri = "pubsub.googleapis.com/projects/${var.project_id}/topics/${local.topic_name}"
  topic_name      = element(concat(google_pubsub_topic.topic.*.name, [""]), 0)
  pubsub_subscriber = element(
    concat(google_service_account.pubsub_subscriber.*.email, [""]),
    0,
  )
  pubsub_subscription = element(
    concat(google_pubsub_subscription.pubsub_subscription.*.id, [""]),
    0,
  )
  pubsub_push_subscription = element(
    concat(google_pubsub_subscription.pubsub_push_subscription.*.id, [""]),
    0,
  )
  subscriber_id = var.subscriber_id == "" ? "${local.topic_name}-subscriber" : var.subscriber_id
}

#Activate Api#

resource "google_project_service" "enable_destination_api" {
  project            = var.project_id
  service            = "pubsub.googleapis.com"
  disable_on_destroy = false
}

#Pub/sub topic#

resource "google_pubsub_topic" "topic" {
  name                       = var.topic_name
  project                    = google_project_service.enable_destination_api.project
  labels                     = var.topic_labels
  message_retention_duration = var.message_retention_duration
  kms_key_name               = var.kms_key_name
}

# Pub/Sub topic subscription #
resource "google_service_account" "pubsub_subscriber" {
  count        = var.create_subscriber ? 1 : 0
  account_id   = local.subscriber_id
  display_name = "${local.topic_name} Topic Subscriber"
  project      = var.project_id
}

resource "google_pubsub_subscription_iam_member" "pubsub_subscriber_role" {
  count        = var.create_subscriber ? 1 : 0
  role         = "roles/pubsub.subscriber"
  project      = var.project_id
  subscription = local.pubsub_subscription
  member       = "serviceAccount:${google_service_account.pubsub_subscriber[0].email}"
}

resource "google_pubsub_topic_iam_member" "pubsub_viewer_role" {
  count   = var.create_subscriber ? 1 : 0
  role    = "roles/pubsub.viewer"
  project = var.project_id
  topic   = local.topic_name
  member  = "serviceAccount:${google_service_account.pubsub_subscriber[0].email}"
}

resource "google_pubsub_subscription" "pubsub_subscription" {
  count   = var.create_subscriber ? 1 : 0
  name    = "${local.topic_name}-subscription"
  project = var.project_id
  topic   = local.topic_name
  labels  = var.subscription_labels
}

resource "google_pubsub_subscription" "pubsub_push_subscription" {
  count   = var.create_push_subscriber ? 1 : 0
  name    = "${local.topic_name}-push-subscription"
  project = var.project_id
  topic   = local.topic_name

  push_config {
    push_endpoint = var.push_endpoint
  }
}