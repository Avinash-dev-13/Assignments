module "pubsub_topic"{
  source                     = "../modules/pub-sub"
  topic_name                 = var.topic_name
  project_id                 = var.project_id
  topic_labels               = var.topic_labels
  message_retention_duration = var.message_retention_duration
  kms_key_name               = var.kms_key_name
  create_subscriber          = var.create_subscriber
  create_push_subscriber     = var.create_push_subscriber
  subscription_labels        = var.subscription_labels
  push_endpoint              = var.push_endpoint
}