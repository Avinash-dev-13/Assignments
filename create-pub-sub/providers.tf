terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.7.0"
    }
  }
  backend "gcs" {
    bucket = "rad_terraform_state_dev" #Your bucket to store tf state file
    prefix = "dev"                     
  }

}
provider "google" {
  project     = var.project_id 
  region      = var.region 
}
